# picocom

- `sudo apt-get install picocom`
- `picocom -b 115200 -r -l /dev/ttyUSB0`
- `picocom /dev/ttyUSB0 --baud 115200 --imap lfcrlf`
To exit picocom, use CNTL-A followed by CNTL-X.
